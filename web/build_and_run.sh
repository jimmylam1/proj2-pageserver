docker build -t cis322 .
docker run -e PYTHONUNBUFFERED=0 -d -p 5000:5000 cis322
echo " "
echo "Checking for running container..."
sleep 1

if docker ps | grep cis322; then
    echo " "
    echo "The container is running!"
else
    echo "ERROR:"
    ./ls_log.sh
fi

