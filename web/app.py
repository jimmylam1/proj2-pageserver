from flask import Flask, render_template, request
import os
import config    # Configure from .ini files and command line

app = Flask(__name__)

DOCROOT = './'  # will be overwritten in main() to get DOCROOT from credentials.ini

# display message if user is in the root directory in the browser
@app.route("/")
def hello():
    return "UOCIS docker demo!"

# For everything else, route url here
@app.route("/<path:url>")
def getFile(url):
    url = os.path.join(DOCROOT, url)  # include DOCROOT in url
    forbidden = ['~', '//', '..']

    # return 403 if a forbidden symbol is in the url
    for symbol in forbidden:
        if symbol in url:
            return error_403(url)

    # try to find the url file
    if '.html' in url or '.css' in url:
        try:
            page = render_template(url)
        # else, file was not found. return 404
        except:
            page = error_404(url)
    else:
        page = error_404(url)

    return page

@app.errorhandler(404)
def error_404(url):
    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(url):
    return render_template('403.html'), 403

def main():
    global DOCROOT  # DOCROOT specifies the directory INSIDE the templates folder

    options = config.configuration()
    DOCROOT = options.DOCROOT

    app.run(debug=True, host='0.0.0.0')


if __name__ == "__main__":
    main()
