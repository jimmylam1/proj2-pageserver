A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

This implements the same functionality as in project 1, but with Flask.

Author: Jimmy Lam, jimmyl@uoregon.edu

# Instructions for running this program

* Build the simple flask app image using

  ```
  docker build -t <some_name> .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 <some_name>
  ```

* Launch http://127.0.0.1:5000 using a web browser and check the output "UOCIS docker demo!"

